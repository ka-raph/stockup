import { TokenService } from "./tokenSvc.js";
import { DataService } from "./dataSvc.js";

const LoginService = {
  check(userName, passWordHash) {
    if (localStorage.getItem(`SU${userName}`) !== null) {
      // Breaks code if null
      if (
        JSON.parse(localStorage.getItem(`SU${userName}`)).passWordHash ===
        passWordHash
      ) {
        return true;
      } else {
        return false;
      }
    } else {
      return false;
    }
  },
  logUser(userName) {
    TokenService.saveToken(userName);
    TokenService.getToken(userName);
    DataService.getUserData(userName);
  },
  logOut(userName) {
    TokenService.removeToken(userName);
    DataService.removeUserData(userName);
  }
};

const CreateAccService = {
  createUser(userName, passWordHash, data) {
    DataService.saveNewUser(userName, passWordHash, data);
  },
  checkExisting(userName) {
    if (localStorage.getItem(`SU${userName}`) !== null) {
      return true;
    } else {
      return false;
    }
  }
};

export { LoginService, CreateAccService };

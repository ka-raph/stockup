import { TokenService } from "./tokenSvc.js";
import store from "../store.js";

const BuySellService = {
  sellStock(userName, stockIndex, newStockAmount, fundsEarned) {
    if (store.state.userToken === TokenService.getToken(userName)) {
      const thisStockIndex = store.state.userData.portfolio.stocks.findIndex(
        function(element) {
          return element.index == stockIndex;
        }
      );
      store.dispatch("sellStock", {
        stockIndex: thisStockIndex,
        newAmount: newStockAmount,
        fundsEarned: fundsEarned
      });
    }
  },
  buyStock(userName, stockIndex, amount, fundsSpent, stockName) {
    if (store.state.userToken === TokenService.getToken(userName)) {
      if (store.state.userData.portfolio.funds > fundsSpent) {
        const thisStockIndex = store.state.userData.portfolio.stocks.findIndex(
          function(element) {
            return element.index == stockIndex;
          }
        );
        if (thisStockIndex !== -1) {
          // Check if the user already possesses stocks from this specific company
          store.dispatch("buyStock", {
            stockIndex: thisStockIndex,
            boughtAmount: amount,
            fundsSpent: fundsSpent,
            new: false
          });
        } else {
          store.dispatch("buyStock", {
            stockIndex: stockIndex,
            boughtAmount: amount,
            fundsSpent: fundsSpent,
            new: true
          });
        }
        alert(`You successfully bought ${amount} stocks of ${stockName} !`);
      } else {
        return alert("You don't have enough funds !");
      }
    }
  },
  checkEnoughStocks(stockIndex, amount) {
    const thisStockIndex = store.state.userData.portfolio.stocks.findIndex(
      function(element) {
        return element.index == stockIndex;
      }
    );
    if (thisStockIndex !== -1) {
      if (
        store.getters.userData.portfolio.stocks[thisStockIndex].quantity >=
        amount
      ) {
        return true;
      } else {
        return false;
      }
    }
  }
};
export { BuySellService };

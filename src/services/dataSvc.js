import { TokenService } from "./tokenSvc.js";
import store from "../store.js";

const DataService = {
  getUserData(userName) {
    if (TokenService.getToken(userName) === store.state.userToken) {
      if (JSON.parse(localStorage.getItem(`SU${userName}`)) !== null) {
        store.dispatch("connectUser", {
          userName: userName,
          data: JSON.parse(localStorage.getItem(`SU${userName}`)).data
        });
      }
    }
  },
  removeUserData() {
    store.dispatch("disconnectUser");
  },
  saveNewUser(userName, passWordHash, data) {
    const newData = {
      passWordHash: passWordHash,
      data: {
        personalData: {
          fName: data.fName,
          lName: data.lName,
          mail: data.mail,
          country: data.country,
          phone: data.phone,
          birth: data.birth
        },
        portfolio: {
          funds: 100000, // Initialise user with 100 000$ funds
          trendValues: [], // So I can display a graph with updated values
          stocks: []
        }
      }
    };
    localStorage.setItem(`SU${userName}`, JSON.stringify(newData));
    /* Now I'll need to push new users in an array so I can update their portfolio's
     value in the backend module */
    let usersList = localStorage.getItem("SUUsersList");
    if (usersList !== null) {
      usersList = JSON.parse(usersList);
      usersList.push(userName);
    } else {
      usersList = [userName];
    }
    localStorage.setItem("SUUsersList", JSON.stringify(usersList));
  },
  updateUserPortfolio(userName) {
    if (TokenService.getToken(userName) === store.state.userToken) {
      var data = JSON.parse(localStorage.getItem(`SU${userName}`));
      data.data.portfolio = store.state.userData.portfolio;
      data.data.portfolio.trendValues = store.state.trendValues; // Update old data with new portfolio
      localStorage.setItem(`SU${userName}`, JSON.stringify(data)); // Return new data to server
    }
  },
  updateUsersTrend(userName) {
    if (localStorage.getItem(`SU${userName}`) == null) {
      return;
    } else {
      store.commit(
        "updateTrendValues",
        JSON.parse(localStorage.getItem(`SU${userName}`)).data.portfolio
          .trendValues
      );
    }
  },
  // GlobalData = stocks data
  getGlobalData() {
    store.commit(
      "updateGlobalData",
      JSON.parse(localStorage.getItem("SUGlobalData"))
    );
  }
};

export { DataService };

import store from "../store.js";

const TokenService = {
  getToken(userName) {
    return localStorage.getItem(`SU${userName}Token`);
  },
  saveToken(userName) {
    /* Will be called when logging in, so we create a new token used to check transactions
         and other operations, also improves security so I don't use a simple constant in order
          to check if anyone is connected */
    const randomToken = Math.floor(Math.random() * 10000).toString();
    localStorage.setItem(`SU${userName}Token`, randomToken);
    store.commit("setToken", randomToken);
  },
  removeToken(userName) {
    // When logging out
    localStorage.removeItem(`SU${userName}Token`);
    store.commit("removeToken");
  }
};

export { TokenService };

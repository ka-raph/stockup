import Vue from "vue";
import Router from "vue-router";
import { TokenService } from "./services/tokenSvc.js";
import store from "./store.js";

const Home = () => import("./views/Home.vue");
const About = () => import("./views/About.vue");
const Login = () => import("./views/Login.vue");
const Signup = () => import("./views/Signup.vue");
const Overview = () => import("./views/Overview.vue");
const Market = () => import("./views/Market.vue");

Vue.use(Router);

const router = new Router({
  mode: "history",
  base: process.env.BASE_URL,
  routes: [
    {
      path: "/",
      name: "home",
      component: Home,
      meta: {
        public: true, // Allow acces to non-logged visitors
        onlyWhenLoggedOut: false // except while logged in
      }
    },
    {
      path: "/login",
      name: "login",
      component: Login,
      meta: {
        public: true,
        onlyWhenLoggedOut: true
      }
    },
    {
      path: "/signup",
      name: "signup",
      component: Signup,
      meta: {
        public: true,
        onlyWhenLoggedOut: true
      }
    },
    {
      path: "/overview",
      name: "overview",
      component: Overview
    },
    {
      path: "/market",
      name: "market",
      component: Market
    },
    {
      path: "/about",
      name: "about",
      component: About,
      meta: {
        public: true
      }
    }
  ]
});

router.beforeEach((to, from, next) => {
  // Check if the page is public, if not, user must be logged in
  /* First, we set the booleans that check if the view can be reached when not logged in and if only while not logged in (login view) */
  const isPublic = to.matched.some(record => record.meta.public);
  const onlyWhenLoggedOut = to.matched.some(
    record => record.meta.onlyWhenLoggedOut
  );
  // I'll use a token
  const userName = store.state.currentUser; // To get a potentially existing token
  let existingToken = "a_nice_token"; // Initialize token with some random text
  if (TokenService.getToken(userName) !== null) {
    existingToken = TokenService.getToken(userName); // Replace the text with the actual user token if it exists
  }
  const loggedIn = existingToken.localeCompare(store.state.userToken) === 0;
  /* If we have a token, it must not be from a previous session (someone trying to take a previously logged user's token) */

  if (!isPublic && !loggedIn) {
    return next({
      path: "/login"
    });
  }

  // Login view can't be visited by logged users
  if (loggedIn && onlyWhenLoggedOut) {
    return next("/");
  }

  next();
});

export default router; // So I can easily push a route to the router

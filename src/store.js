import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    userToken: "",
    currentUser: "",
    userData: {},
    SUGlobalData: [],
    trendValues: []
  },
  mutations: {
    setUserName(state, userName) {
      state.currentUser = userName;
    },
    setUserData(state, data) {
      state.userData = data;
    },
    setToken(state, token) {
      state.userToken = token;
    },
    removeToken(state) {
      state.userToken = "";
    },
    updateGlobalData(state, newGlobalData) {
      state.SUGlobalData = newGlobalData;
    },
    updateUserPortfolio(state, stocks) {
      state.userData.portfolio.stocks = stocks;
    },
    sellStock(state, stock) {
      // If selling stocks with still some of that company left
      state.userData.portfolio.stocks[stock.stockIndex].quantity =
        stock.newAmount;
    },
    sellAllStock(state, stockIndex) {
      // If selling all the owned stocks from one company
      state.userData.portfolio.stocks.splice(stockIndex, 1);
    },
    earnMoney(state, fundsEarned) {
      state.userData.portfolio.funds += fundsEarned;
    },
    buyExistingStock(state, stock) {
      state.userData.portfolio.stocks[stock.stockIndex].quantity +=
        stock.boughtAmount;
    },
    buyNewStock(state, stock) {
      let key = stock.stockIndex * 3; // I need a new and unique key to automatically render those in the overview view
      state.userData.portfolio.stocks.push({
        index: stock.stockIndex,
        quantity: stock.boughtAmount,
        key: key
      });
      /* Only those informations needed. The index will allow me to easily
            find the right company in the database to get the rest of the info */
    },
    spendMoney(state, fundsSpent) {
      state.userData.portfolio.funds -= fundsSpent;
    },
    updateTrendValues(state, array) {
      state.trendValues = array;
    }
  },
  actions: {
    sellStock({ commit }, payload) {
      if (payload.newAmount > 0) {
        commit("sellStock", payload);
        commit("earnMoney", payload.fundsEarned);
      } else {
        commit("sellAllStock", payload.stockIndex);
        commit("earnMoney", payload.fundsEarned);
      }
    },
    buyStock({ commit }, payload) {
      if (payload.new != true) {
        commit("buyExistingStock", payload);
        commit("spendMoney", payload.fundsSpent);
      } else {
        commit("buyNewStock", payload);
        commit("spendMoney", payload.fundsSpent);
      }
    },
    connectUser({ commit }, payload) {
      commit("setUserName", payload.userName);
      commit("setUserData", payload.data);
    },
    disconnectUser({ commit }) {
      commit("setUserName", "");
      commit("setUserData", {});
    }
  },
  getters: {
    userToken: state => state.userToken,
    currentUser: state => state.currentUser,
    userData: state => state.userData,
    SUGlobalData: state => state.SUGlobalData,
    trendValues: state => state.trendValues
  }
});

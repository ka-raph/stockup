import store from "../store.js";

const BackendMethods = {
  getUserList() {
    const usersList = localStorage.getItem("SUUsersList");
    if (usersList !== null) {
      return JSON.parse(usersList);
    } else {
      return false;
    }
  },
  updatePortfolioValues(userName, currentTotalValue) {
    // currentTotalValue will be calculated inside backendModule for more conveniency
    let trendValues = JSON.parse(localStorage.getItem(`SU${userName}`)).data
      .portfolio.trendValues;
    trendValues.push(currentTotalValue);
    let userData = JSON.parse(localStorage.getItem(`SU${userName}`));
    userData.data.portfolio.trendValues = trendValues;
    localStorage.setItem(`SU${userName}`, JSON.stringify(userData));
  },
  removeGlobalData() {
    store.commit("updateGlobalData", []);
  }
};

export { BackendMethods };

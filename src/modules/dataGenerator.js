const DataGeneratorService = {
  generateStockPrice(name) {
    // The function given on the exercise
    var hash = name.split("").reduce(function(a, b) {
      a = (a << 5) - a + b.charCodeAt(0);
      return a & a;
    }, 0);
    var amplitude = 100;
    var d = new Date();
    var n = d.getTime();
    return (
      Math.round(
        Math.abs(3 * Math.sin(2 * hash * n) + Math.random() * amplitude) * 1000
      ) / 1000
    );
  },
  initialStocks() {
    // Setup initial stocks if ever they don't already exist (can keep track of it on a same browser)
    if (localStorage.getItem("SUGlobalData") !== null) {
      return;
    } else {
      let data = [];
      for (let i = 0; i < 10; ++i) {
        let stock = {
          name: `Company${i}`,
          shortName: `CO${i}`,
          value: 0,
          key: i * 2 + 1, // To render them later
          index: i, // To find it more easily in "complex" methods
          valuesArray: []
        };
        stock.value = this.generateStockPrice(stock.name);
        data.push(stock);
      }
      return localStorage.setItem("SUGlobalData", JSON.stringify(data));
      /* Save data on server */
    }
  },
  updateStocksValues() {
    // This one -> looped to upodate all values
    let stocks = JSON.parse(localStorage.getItem("SUGlobalData"));
    for (let i = 0; i < stocks.length; ++i) {
      const newValue = this.generateStockPrice(stocks[i].name);
      stocks[i].value = newValue;
      stocks[i].valuesArray.push(newValue);
    }
    return localStorage.setItem("SUGlobalData", JSON.stringify(stocks));
  }
};

export { DataGeneratorService };

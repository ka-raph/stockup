# stockup

Welcome to StockUp, a mockup of an online trading platform

## Data model
```
SU'userName' : {
    passWordHash: passWordHash, // MD5 encription
      data: {
        personalData: {
          fName: first name,
          lName: last name,
          mail: mail,
          country: country,
          phone: phone,
          birth: birth
        },
        portfolio: {
          funds: 100000, // Initialise user with 100 000$ funds
          trendValues: [], // So I can display a graph with updated values
          stocks: [] // User's stocks
        }
      }
}
```

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
